import React, { useEffect, useState } from "react";
import { Button, Col, Container, Row } from "react-bootstrap";
import { Link } from "react-router-dom";
import Swal from "sweetalert2";
import { api } from "../api/api";
import CardCategory from "../component/CardCategory";

export default function CategoryComponent() {
  const [category, setCategory] = useState([]);

  useEffect(() => {
    api.get("/category").then((res) => {
      setCategory(res.data.payload);
      console.log(res);
    });
  }, []);

  const handleDelete = (id) => {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: "btn btn-success",
        cancelButton: "btn btn-danger",
      },
      buttonsStyling: false,
    });

    swalWithBootstrapButtons
      .fire({
        title: "Are you sure?",
        text: "You won't be able to revert this!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No, cancel!",
        reverseButtons: true,
      })
      .then((result) => {
        let message = "Your file has been deleted.";
        if (result.isConfirmed) {
          const newData = category.filter((data) => data._id !== id);
          setCategory(newData);
          api.delete(`/category/${id}`).then((r) => {
            console.log(r.data.message);
          });

          swalWithBootstrapButtons.fire("Deleted!", message, "success");
        } else {
          return;
        }
      });
  };

  return (
    <div>
      <Container>
        <div className="d-flex p-3 justify-content-between">
          <h1>All Categories</h1>
          <Button
            as={Link}
            to="/category/create"
            className="btn-light text-center align-self-center fw-bold"
          >
            New Category
          </Button>
        </div>
        <Row>
          {category.map((item, index) => (
            <Col xs={6} sm={3} md={2} key={index}>
              <CardCategory item={item} handleDelete={handleDelete} />
            </Col>
          ))}
        </Row>
      </Container>
    </div>
  );
}
