import React from "react";
import { Container } from "react-bootstrap";

function NotFoundCompoent() {
  return (
    <Container>
      <h1>Opp! Something Wrong 😢😢</h1>
    </Container>
  );
}

export default NotFoundCompoent;
