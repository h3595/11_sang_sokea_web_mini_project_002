import React from "react";
import { Button, Container, Image, Row } from "react-bootstrap";
import { useLocation, useNavigate } from "react-router-dom";
import "../css/styles.css";

export default function ViewComponent() {
  const location = useLocation();
  const oldData = location.state;
  console.log(oldData);
  const navigate = useNavigate();
  return (
    <>
      <Container className="my-2">
        <Row className="view-card p-3">
          <Button
            onClick={() => navigate("/")}
            style={{ width: "10%" }}
            className="m-3 btn-light fw-bold"
          >
            Back
          </Button>
          <div
            className=""
            style={{ display: "flex", margin: 5, height: "500px" }}
          >
            <Image
              src={
                oldData.image ??
                "https://socialistmodernism.com/wp-content/uploads/2017/07/placeholder-image.png?w=640"
              }
              width="50%"
              height="100%"
              style={{ flex: 1, borderRadius: "20px", objectFit: "cover" }}
              className=""
            />
            <div
              style={{
                flex: 1,
                height: "100%",
                flexWrap: "wrap",
                width: "100vw",
                position: "relative",
                overflow: "hidden",
                wordBreak: "break-word",
              }}
            >
              <h4 className="p-3">Title: {oldData?.title}</h4>
              <p style={{ position: "relative", flex: 1 }} className="p-3">
                Description: {oldData?.description}
              </p>
              <p className="p-3">
                Published: {oldData?.published ? "True" : "False"}
              </p>
            </div>
          </div>
        </Row>
      </Container>
    </>
  );
}
