import axios from "axios";
import React, { useEffect, useState } from "react";
import { Button, Container, Form } from "react-bootstrap";
import Swal from "sweetalert2";
import { api } from "../api/api";
import { Link, useLocation, useNavigate } from "react-router-dom";

export default function CreateCategory() {
  const [name, setTitle] = useState("");
  const navigate = useNavigate();
  const beforSubmit = () => {
    Swal.fire({
      title: "Are you sure?",
      text: "You will saved this Category!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, save it!",
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire("Save!", "Your file has been Save.", "success");
        handleSubmit();
        navigate("/category");
      } else {
        return;
      }
    });
  };

  const handleSubmit = () => {
    api
      .post("/category", { name })
      .then((res) => Swal.fire("Good job!", res?.data?.message, "success"));
  };

  const handleNameChange = (e) => {
    setTitle(e.target.value);
  };

  useEffect(() => {
    console.log(name);
  }, [name]);

  return (
    <Container className="w-50">
      <h1>Add New Category</h1>
      <Form>
        <Form.Group className="mb-3" controlId="formBasicEmail">
          <Form.Label>Title</Form.Label>
          <Form.Control
            type="text"
            placeholder="name"
            onChange={handleNameChange}
          />
        </Form.Group>
        <Button variant="danger mx-2" as={Link} to="/category">
          Canncel
        </Button>
        <Button variant="primary" onClick={beforSubmit}>
          Submit
        </Button>
      </Form>
    </Container>
  );
}
