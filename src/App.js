import logo from "./logo.svg";
import "./App.css";
import { Route, Routes } from "react-router-dom";
import { HomeComponent } from "./pages/HomeComponent";
import NavBarComponent from "./component/NavBarComponent";
import CreatePostComponent from "./pages/CreatePostComponent";
import UpdateComponent from "./pages/UpdateComponent";
import ViewComponent from "./pages/ViewComponent";
import CategoryComponent from "./pages/CategoryComponent";
import CreateCategory from "./pages/CreateCategory";
import UpdateCategory from "./pages/UpdateCategory";
import ViewCategory from "./pages/ViewCategory";
import NotFoundCompoent from "./pages/NotFoundCompoent";

function App() {
  return (
    <div className="App">
      <NavBarComponent />
      <Routes>
        <Route path="/" element={<HomeComponent />} />
        <Route path="/create" element={<CreatePostComponent />} />
        <Route path="/category" element={<CategoryComponent />} />
        <Route path="/category/create" element={<CreateCategory />} />
        <Route path="/update" element={<UpdateComponent />} />
        <Route path="/category/update" element={<UpdateCategory />} />
        <Route path="/view" element={<ViewComponent />} />
        <Route path="/category/view" element={<ViewCategory />} />
        <Route path="*" element={<NotFoundCompoent />} />
      </Routes>
    </div>
  );
}

export default App;
