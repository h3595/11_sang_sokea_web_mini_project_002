import React from "react";
import { Button, Card } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import "../css/styles.css";
export default function CardCategory({ item, handleDelete }) {
  const navigate = useNavigate();
  const onUpdate = (name, id) => {
    navigate("/category/update", { state: { name, id} });
  };

  const onView = () => {
    navigate("/category/view", { state: { ...item } });
  };

  return (
    <>
      <Card className="my-2 " style={{ borderRadius: "20px" }}>
        <Card.Body>
          <Card.Title style={{ height: "50px" }} className="title">
            {item.name}
          </Card.Title>
          <div className="d-flex flex-column mb-2">
            <Button className="mb-2" onClick={onView} variant="primary">
              View
            </Button>
            <Button
              className="mb-2"
              variant="danger"
              onClick={() => handleDelete(item._id)}
            >
              Delete
            </Button>
            <Button variant="info" onClick={() => onUpdate(item.name, item._id)}>
              Update
            </Button>
          </div>
        </Card.Body>
      </Card>
    </>
  );
}
